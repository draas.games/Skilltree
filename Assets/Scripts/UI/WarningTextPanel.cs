using System.Collections;
using UnityEngine;
using TMPro;
using SkillTest.Skills;

namespace SkillTest.UI
{
    public class WarningTextPanel : MonoBehaviour
    {
        [SerializeField] private SkillPanelInfo skillPanelInfo;
        [SerializeField] private TextMeshProUGUI warningText;

        private WaitForSeconds textTimer = new WaitForSeconds(1f);
        private string defaultText = "Жду ошибку...";
        
        private void Start()
        {
            skillPanelInfo.OnWarningDisplaying += DisplayWarningMessage;
        }

        private void DisplayWarningMessage(string message)
        {
            warningText.text = message;
            StartCoroutine(FadeMessage());
        }

        private IEnumerator FadeMessage()
        {
            yield return textTimer;
            warningText.text = defaultText;
        }


    }
}
