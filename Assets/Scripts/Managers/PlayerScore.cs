using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SkillTest.Skills;

namespace SkillTest.Core
{
    public class PlayerScore : MonoBehaviour
    {
        public TextMeshProUGUI scoreText;
        public Button earnButton;
        
        public int Score { get; private set; }

        public static event Action<SkillData> OnSkillAdding;

        public static event Action OnSkillChanged; 

        private void Awake()
        {
            SkillPanelInfo.OnAddSkillClicked += CheckAddSkillCondition;

            PlayerSkillsManager.OnSkillRemoved += CheckRemoveSkillCondition;
        }
        
        private void Start()
        {
            UpdateText();
            earnButton.onClick.AddListener(Earn);
        }

        private void Earn()
        {
            AddScore(1);
        }
        
        private void UpdateText()
        {
            scoreText.text = "Ваше количество очков: " + Score;
        }

        private void CheckAddSkillCondition(SkillData skillData)
        {
            if(skillData.skillCost > Score)
                 return;

            RemoveScore(skillData.skillCost);
            OnSkillAdding?.Invoke(skillData);
        }

        private void CheckRemoveSkillCondition(SkillData skillData)
        {
            AddScore(skillData.skillCost);
            OnSkillChanged.Invoke();
        }

        private void AddScore(int value)
        {
            Score += Math.Abs(value);
            UpdateText();
        }

        private void RemoveScore(int value)
        {
            Score -= Math.Abs(value);
            UpdateText();
        }
    }
}
