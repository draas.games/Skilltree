using System;
using UnityEngine;
using UnityEngine.UI;
using SkillTest.Skills;

namespace SkillTest.Core
{
    
    public class PlayerSkillsManager : MonoBehaviour
    {
        public PlayerSkillList skills;
        public Button forgetAllButton;
        
        public static event Action<SkillData> OnSkillRemoved;
        public static event Action OnAllSkillsRemoved;
        
        private void Awake()
        {
            SkillPanelInfo.OnRemoveSkillClicked += RemoveSkill;
            PlayerScore.OnSkillAdding += AddSkill;
        }

        private void Start()
        {
            forgetAllButton.onClick.AddListener(RemoveAllSkills);
        }

        private void AddSkill(SkillData skill)
        {
            skill.skillOwned = true;
            skills.skillDataList.Add(skill);
        }
        
        private void RemoveSkill(SkillData skillData)
        {
            if(!CheckSkill(skillData))
                return;

            skills.skillDataList.Remove(skillData);
            skillData.skillOwned = false;
            OnSkillRemoved?.Invoke(skillData);
        }

        private void RemoveAllSkills()
        {
            //remove in reverse
            for (int i = skills.skillDataList.Count - 1; i >= 0; i--)
            {
                if (skills.skillDataList[i].skillType != SkillType.Base)
                {
                    RemoveSkill(skills.skillDataList[i]);
                }
            }

            OnAllSkillsRemoved.Invoke();
        }
        
        private bool CheckSkill(SkillData skill)
        {
            if (skills.skillDataList.Contains(skill))
                return true;
            
            return false;
        }
        
    }
}
