using System.Collections.Generic;
using UnityEngine;

namespace SkillTest.Skills
{
    [CreateAssetMenu(menuName = "Player/Player skill list", fileName = "PlayerSkillList", order = 1)]
    public class PlayerSkillList : ScriptableObject
    {
        public List<SkillData> skillDataList;
    }
}
