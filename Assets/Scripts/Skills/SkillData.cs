using UnityEngine;

namespace SkillTest.Skills
{
    public enum SkillType
    {
        Base, 
        Learnable
    }

    [CreateAssetMenu(menuName = "Data/Skill data", fileName = "Skill data", order = 1)]
    public class SkillData : ScriptableObject
    {
        public bool skillOwned = false;
        public SkillType skillType = SkillType.Learnable;
        
        public string skillName;
        [TextArea(3, 7)]
        public string skillDescription;
        
        public int skillCost;
    }

}
