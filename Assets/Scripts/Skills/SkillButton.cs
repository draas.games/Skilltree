using System;
using UnityEngine;
using UnityEngine.UI;
using SkillTest.Core;

namespace SkillTest.Skills
{
    public class SkillButton : MonoBehaviour
    {
        private Button skillButton;
        [SerializeField] private SkillData skillData;
        public SkillData SkillData
        {
            get { return skillData; }
        }
        
        public SkillButton[] connectedSkills;

        public static event Action<SkillButton> OnSkillButtonClicked;
        
        private void Start()
        {
            skillButton = GetComponent<Button>();
            skillButton.onClick.AddListener(ActivateSkillInfoPanel);

            ChangeConnectedButtonsState();

            PlayerScore.OnSkillChanged += ChangeConnectedButtonsState;
            PlayerSkillsManager.OnAllSkillsRemoved += ChangeConnectedButtonsState;
        }

        public void ChangeConnectedButtonsState()
        {
            foreach (var button in connectedSkills)
            {
                if (this.skillData.skillOwned)
                {
                    button.gameObject.GetComponent<Button>().interactable = true;
                }
                else button.gameObject.GetComponent<Button>().interactable = false;
            }
        }
        
        private void ActivateSkillInfoPanel()
        {
            ChangeConnectedButtonsState();
            OnSkillButtonClicked?.Invoke(this);
        }

    }
}