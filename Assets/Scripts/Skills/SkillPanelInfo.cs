using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SkillTest.Skills
{
    public class SkillPanelInfo : MonoBehaviour
    {
        
        [SerializeField] private Canvas canvas;
        
        [SerializeField] private SkillData currentSkillData;
        
        [SerializeField] private Button addSkillButton;
        [SerializeField] private Button removeSkillButton;
        [SerializeField] private Button closePanelButton;

        [SerializeField] private TextMeshProUGUI skillNameText;
        [SerializeField] private TextMeshProUGUI skillDescriptionText;
        [SerializeField] private TextMeshProUGUI skillPriceText;
        
        private SkillButton currentButton;
        private SkillButton[] skillButtons;

        public static event Action<SkillData> OnAddSkillClicked;
        public static event Action<SkillData> OnRemoveSkillClicked;
        
        public delegate void DisplayWarning(string text);
        public event DisplayWarning OnWarningDisplaying;
        
        public void Start()
        {
            SkillButton.OnSkillButtonClicked += EnablePanel;
            
            addSkillButton.onClick.AddListener(TryBuySkill);
            removeSkillButton.onClick.AddListener(TryRemoveSkill);
            closePanelButton.onClick.AddListener(DisablePanel);
            
            DisablePanel();
        }

        private void EnablePanel(SkillButton skillButton)
        {
            var skillData = skillButton.SkillData;
            currentButton = skillButton;
            
            currentSkillData = skillData;
            skillNameText.text = skillData.skillName;
            skillDescriptionText.text = skillData.skillDescription;
            skillPriceText.text = skillData.skillCost.ToString();

            RefreshButtons();
            
            canvas.enabled = true;
        }

        private void RefreshButtons()
        {
            if (IsBaseSkill()) 
            {
                addSkillButton.interactable = false;
                removeSkillButton.interactable = false;
                skillPriceText.text = String.Empty;
                return;
            }
            
            if (currentSkillData.skillOwned)
            {
                addSkillButton.interactable = false;
                removeSkillButton.interactable = true;
            }
            else
            {
                addSkillButton.interactable = true;
                removeSkillButton.interactable = false;
            }
        }

        private bool NextSkillsOwned()
        {
            foreach (var skillButton in currentButton.connectedSkills)
            {
                if(skillButton.SkillData.skillOwned)
                    return false;
            }

            return true;
        }

        private bool IsBaseSkill()
        {
            if (currentButton.SkillData.skillType == SkillType.Base)
                return true;

            return false;
        }
        
        private void DisablePanel()
        {
            canvas.enabled = false;
        }
        
        private void TryBuySkill()
        {
            OnAddSkillClicked?.Invoke(currentSkillData);
            RefreshButtons();
            currentButton.ChangeConnectedButtonsState();
        }

        private void TryRemoveSkill()
        {
            string errorMessage =
                "У вас приобретены следующие навыки. Пожалуйста, прежде чем забыть этот, забудьте и следующие.";

            if (!NextSkillsOwned())
            {
                OnWarningDisplaying?.Invoke(errorMessage);
                return;
            }

            OnRemoveSkillClicked?.Invoke(currentSkillData);
            RefreshButtons();
            currentButton.ChangeConnectedButtonsState();
        }
    }
}
